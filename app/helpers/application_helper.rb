module ApplicationHelper
	def bootstrap_class_for(flash_type)
		{ success: 'alert-success', error: 'alert-danger', alert: 'alert-warning',
			notice: 'alert-info' }[flash_type.to_sym] || flash_type.to_s
	end

	def bootstrap_icon_for(flash_type)
		{ success: 'ok-circle', error: 'remove-circle', alert: 'warning-sign',
			notice: 'exclamation-sign' }[flash_type] || 'question-sign'
	end

	def full_title(page_title = '', base_title = 'Spectacle')
	  if page_title.empty?
	    base_title
	  else
	    page_title + " | " + base_title
	  end
	end
end
